const path  = require("path");
const fs  = require("fs");
const Log = require("../db/model/log")

let pathsMapping = {};

function loadEndPoints() {
    return new Promise((resolve, reject) => {
        try {
            let allControllersPaths = [];
            let pathModule = path.resolve(__dirname, '../', 'routes');

            let findControllers = (currentPath) => {
                let files = fs.readdirSync(currentPath);

                for (let fileName of files) {
                    let currentPathFile = path.resolve(currentPath, fileName);
                    if (/(.js)$/.test(fileName)) {
                        allControllersPaths.push(currentPathFile);
                    } else if (fs.statSync(currentPathFile).isDirectory()) {
                        findControllers(currentPathFile);
                    }
                }
            };

            findControllers(pathModule);

            for (let controllerPath of allControllersPaths) {
                require(controllerPath);
            }

            resolve();
        } catch (error) {
            reject(error);
        }
    });
}

function EndPoint(config) {

    if (!config.route) {
        throw new Error('Configuração:route é obrigatória');
    }

    if (!config.verb) {
        throw new Error('Configuração:verb é obrigatória');
    }

    if (!config.response) {
        throw new Error('Configuração:response é obrigatória');
    }

    return function (Target, methodName) {
        let callback = (req, res) => {
            try {
                let paramsRequest = req.getter.getParams();

                if (config.params) {
                    paramsRequest = validateEntryParams(config.params, paramsRequest);
                }

                let log = new Log({
                    sistema: paramsRequest.system || req.url || '',
                    url: req.url || '',
                    metodo: methodName,
                    fonte: paramsRequest.service || ''
                })
                log.save()
                new Target(req, res)[methodName](paramsRequest)
                    .then(
                        res.sender.end.bind(res.sender),
                        res.sender.error.bind(res.sender),
                        res.sender.write.bind(res.sender)
                    )
                    .catch(error => {
                        console.error(error);
                        res.sender.error.bind(res.sender);
                    });
            } catch (error) {
                res.sender.error(error);
            }
        };

        let verbs = config.verb instanceof Array ? config.verb : [config.verb];

        for (let verb of verbs) {
            if (!pathsMapping[config.route.toString()]) {
                pathsMapping[config.route.toString()] = {};
            }
            pathsMapping[config.route.toString()][verb] = config;
            server.putRoute(verb, config.route, callback);
        }
    }
}

function getConfigurations () {
    return pathsMapping;
}

function validateEntryParams(config = {}, paramsRequest) {
    for (let paramName in config) {
        if (config.hasOwnProperty(paramName)) {
            let paramDefinitions = config[paramName];
            let value = typeof paramsRequest[paramName] !== 'undefined' ? paramsRequest[paramName] : paramDefinitions.default;
            value = validateRequired(paramDefinitions, paramName, value);

            if (paramDefinitions.required === true || value !== void 0) {
              value = validateType(paramDefinitions, paramName, value);
              value = validateCast(paramDefinitions, paramName, value);
              value = validateENUM(paramDefinitions, paramName, value);
              value = execValidate(paramDefinitions, paramName, value);
            }

            paramsRequest[paramName] = value;
        }
    }

    return paramsRequest;
}

function execValidate(paramDefinitions, paramName, value) {
    if (typeof paramDefinitions.validate === 'function') {
        return paramDefinitions.validate(value);
    }
    return value;
}

function validateRequired(paramDefinitions, paramName, value) {
    if (paramDefinitions.required) {
        if (value === void 0) {
            throw new Error(`O parâmetro \'${paramName}\' é obrigatório`);
        }
    }
    return value;
}

function validateCast(paramDefinitions, paramName, value) {
    if (paramDefinitions.type === 'object' || paramDefinitions.type === 'array') {
        value = typeof value === 'string' ? JSON.parse(value) : value;
    } else {
        if (paramDefinitions.type ==='boolean' && typeof value !== 'boolean'){
            value = eval(`${paramDefinitions.type.replace(/(.)/, (a, b) => b.toUpperCase())}("${value}")`);
        }
    }
    return value;
}

function validateENUM(paramDefinitions, paramName, value) {
    if (paramDefinitions.enum instanceof Array) {
        if (paramDefinitions.enum.indexOf(value) < 0) {
            throw new Error(`O parâmetro ${paramName} está inválido`);
        }
    }
    return value;
}

function validateType(paramDefinitions, paramName, value) {
    if (paramDefinitions.type) {
        if (paramDefinitions.type === 'array') {
            if (!(value instanceof Array)) {
                throw new Error(`O parâmetro ${paramName} está inválido`);
            }

        } else if (!paramDefinitions.type.split('|').includes(typeof value)) {
            throw new Error(`O parâmetro ${paramName} está inválido`);

        }
    }
    return value;
}

exports.EndPoint = EndPoint;
exports.getConfigurations = getConfigurations;
exports.validateEntryParams = validateEntryParams;
exports.loadEndPoints = loadEndPoints;
