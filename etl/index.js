const config = require("../config");
const Store = require("../Store");
const {GeoprocessorController} = require("../controller/GeoprocessorController");

class ETL {

  load() {
    let servicePontosDeFronteira = Store.get('service:pontosDeFronteira');
    let serviceLimitesBrasil = Store.get('service:limitesBrasil');
    let geoProcessor = new GeoprocessorController();

    return Promise.all([
      geoProcessor.query(servicePontosDeFronteira, {
        returnGeometry: true,
        where: '1=1',
        outSR: 4326
      }),
      geoProcessor.query(serviceLimitesBrasil, {
        returnGeometry: true,
        where: '1=1',
        outSR: 4326
      })
    ])
      .then((results) => {
        Store.set('pontosDeFronteira', results[0].features);
        return results[1];
      })
      .then((geoJSON) => {
        Store.set('limitesBrasil', geoJSON.features[0]);
        return geoJSON;
      });
  }

}

module.exports = new ETL();