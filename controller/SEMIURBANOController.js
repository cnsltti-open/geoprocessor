const GeoExecutor = require("../helper/GeoExecutor")
const {messages} = require("../messages")
const {GeometryType} = require("../enum/GeometryType")
const {SpatialRelEnum} = require("../enum/SpatialRelEnum")
const Store = require("../Store")

class SEMIURBANOController {

  constructor(request, response) {
    this.request = request;
    this.response = response;
    this.system = 'SEMIURBANO'
  }

  getDirections(params) {
    return new Promise((resolve, reject) => {
      let estadosBrasil = Store.get('service:estados')
      let originUF
      let destinationUF
      let campoNomeUF = estadosBrasil.campoNomeUF

      let defaulParams = {
        serviceId: estadosBrasil.id,
        outFields: [estadosBrasil.campoNomeUF],
        geometryType: GeometryType.POINT,
        spatialRel: SpatialRelEnum.Intersects,
        returnGeometry: false,
        inSR: 4326,
        outSR: 4326,
        system: this.system
      }

      //Consome serviço para pegar as UFs que intersectam com os pontos de origem e destino final
      return Promise.all([
        new GeoExecutor(this.request, this.response).exec(estadosBrasil, 'query', Object.assign({}, defaulParams, {
          geometry: params.origin
        }), false),
        new GeoExecutor(this.request, this.response).exec(estadosBrasil, 'query', Object.assign({}, defaulParams, {
          geometry: params.destination
        }), false)
      ]).then(geoJSONs => {
        // Verifica se os pontos de inicio e fim estão na mesma UF
        try {
          if (geoJSONs[0].features.length &&
            geoJSONs[0].features[0].properties &&
            geoJSONs[0].features[0].properties[campoNomeUF]
          ) {
            originUF = geoJSONs[0].features[0].properties[campoNomeUF];
          }

          if (geoJSONs[1].features.length &&
            geoJSONs[1].features[0].properties &&
            geoJSONs[1].features[0].properties[campoNomeUF]
          ) {
            destinationUF = geoJSONs[1].features[0].properties[campoNomeUF];
          }

          if (originUF === destinationUF) {
            throw new Error(messages.M0001);
          }

        } catch (error) {
          console.error(error);
          throw new Error(messages.M0001);
        }
      }).then(() => {
        params.system = this.system
        return new GeoExecutor(this.request, this.response)
        .exec(params.service, 'getDirections', params)
        .then((geoJSONMultipleFeatures) => {
          if (!geoJSONMultipleFeatures.length) {
            resolve({})
          } else {
            // Transforma as feições do geoJSON em uma feição só.

            let simpleGeoJSON = {
              geometry: {
                coordinates: geoJSONMultipleFeatures[0].geometry.coordinates,
                type: 'LineString'
              },
              properties: {
                distance: geoJSONMultipleFeatures[0].properties.distance.value,
                duration: geoJSONMultipleFeatures[0].properties.duration.value,
                end_address: geoJSONMultipleFeatures[0].properties.end_address,
                start_address: geoJSONMultipleFeatures[0].properties.start_address
              },
              steps: geoJSONMultipleFeatures[0].steps,
              type: 'Feature'
            }
            let feature

            for (let i = 1, len = geoJSONMultipleFeatures.length; i < len; i++) {
              feature = geoJSONMultipleFeatures[i]

              simpleGeoJSON.geometry.coordinates = simpleGeoJSON.geometry.coordinates.concat(feature.geometry.coordinates)
              simpleGeoJSON.steps = simpleGeoJSON.steps.concat(feature.steps)
              simpleGeoJSON.properties.distance += feature.properties.distance.value
              simpleGeoJSON.properties.duration += feature.properties.duration.value
              simpleGeoJSON.properties.end_address = feature.properties.end_address
            }

            simpleGeoJSON.properties.distance = this._getDistanceText(simpleGeoJSON.properties.distance)
            simpleGeoJSON.properties.duration = this._getDurationText(simpleGeoJSON.properties.duration)
            simpleGeoJSON.properties.steps = simpleGeoJSON.steps

            resolve(simpleGeoJSON)
          }
        })
      }).catch(err => {
        console.log(err)
        reject(err)
      })
    })
  }

  _getDurationText (value) {
    let type = ' min'
    let val
    if (value < 60) {
      val = 1
    } else if (value < 3600) {
      val = parseInt(value / 60) + 1
    } else if (value < 86400) {
      val = parseInt(value / 3600) + 1
      type = ' h'
    } else if (value < 2592000) {
      val = parseInt(value / 86400) + 1
      type = ' d'
    } else {
      val = parseInt(value / 2592000) + 1
      type = ' m'
    }

    return val + type
  }

  _getDistanceText (value) {
    let type = ' m'
    let val
    if (value < 1000) {
      val = value
    } else {
      val = parseInt(value / 1000)
      type = ' km'
    }

    return val + type
  }
}

exports.SEMIURBANOController = SEMIURBANOController
