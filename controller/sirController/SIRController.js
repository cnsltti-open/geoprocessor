const config = require("../../config")
const CsvFileResult = require('./CsvFileResult')
const { default: pointToLine } = require('@turf/point-to-line-distance')
const Papa = require('papaparse')
const axios = require('axios')

class SIRController {

  constructor(request, response) {
    this.request = request;
    this.response = response;
    this.system = 'SIR'
  }
  /**
   * @typedef {Object} MapaSir~GetPointsFromCsvParams
   * @property {File} file - Arquivo CSV com pontos.
   * @property {MapaSir~LoadingCallback} [loading=null] - O handler do loading.
   * @property {string} [latCol='Latitude'] - A coluna de latitude de cada ponto.
   * @property {string} [lonCol='Longitude'] - A coluna de longitude de cada ponto.
   */

  /**
   * @typedef {Object} MapaSir~GetRodoviasFilterFromParams
   * @property {CsvFileResult} [csvResult=null] - O resultado de uma leitura CSV.
   */

  /**
   * @typedef {Object} MapaSir~GetRodoviasFeaturesParams
   * @property {string} [rodoviasFilter=null] - O filtro da query de rodovias.
   */

  /**
   * @typedef {Object} MapaSir~PlotRodoviasParams
   * @property {RodoviasResponse} rodoviasFeatures - A camada atual de rodovias.
   */

  /**
   * @typedef {Object} MapaSir~CategorizeCsvResultsParams
   * @property {CsvFileResult} [csvResult=null] - O resultado da leitura CSV.
   * @property {RodoviasResponse} rodoviasFeatures - As feições das rodovias.
   * @property {number} distanceToRodovias - A distância para categorizar
   * entre perto e longe das rodovias.
   */

  /**
   * @typedef {Object} MapaSir~CategorizedCsvPoints
   * @property {object[]} inside - Os pontos que estão dentro da distância
   * limite das rodovias.
   * @property {object[]} outside - Os pontos que estão fora da distância
   * limite das rodovias.
   */

  /**
   * @typedef {Object} MapaSir~PlotCategorizedCsvPointsParams
   * @property {MapaSir~CategorizedCsvPoints} pointsCategorized - Os
   * pontos do CSV categorizados pelas rodovias.
   */

  /**
   * @typedef {Object} MapaSir~ZoomOnRodoviasParams
   * @property {L.GeoJSON} rodoviasLayer - A camada de rodovias.
   */

  /**
   * @typedef {Object} MapaSir~CurrentState
   * @property {File} file - O último arquivo CSV com pontos a ser lido.
   * @property {number} distanceToRodovias - A distância para categorizar
   * entre perto e longe das rodovias.
   * @property {MapaSir~LoadingCallback} [loading=null] - O handler do
   * loading atual.
   * @property {string} [latCol='Latitude'] - A coluna de latitude de
   * cada ponto.
   * @property {string} [lonCol='Longitude'] - A coluna de longitude de
   * cada ponto.
   * @property {CsvFileResult} [csvResult=null] - O último resultado de
   * uma leitura CSV.
   * @property {string} [rodoviasFilter=null] - O filtro atual da query
   * de rodovias.
   * @property {RodoviasResponse} rodoviasFeatures - As feições atuais
   * de rodovias.
   * @property {MapaSir~CategorizedCsvPoints} pointsCategorized - Os
   * pontos do CSV categorizados pelas rodovias.
   * @property {L.Marker[]} markersOnMap - Os marcadores que estão no
   * mapa.
   */

  /**
   * Metodo para plotar no mapa os pontos de CSV pertos de rodovias.
   * @param {Object} params - Os parâmetros da requisição.
   * @returns {Promise<MapaSir~CategorizedCsvPoints>} Uma promise que
   * quando finalizada retorna os pontos do CSV categorizados pelas
   * rodovias.
   */
  async categorizePointsOfRodovias (params) {
    const {
      file,
      distanceToRodovias,
      latCol,
      lonCol
    } = params
    const data = {
      file,
      distanceToRodovias: distanceToRodovias / 1000,
      latCol,
      lonCol
    }
    await this.getPointsFromCsv(data)
    this.getRodoviasFilterFromCsvResult(data)
    await this.getRodoviasFeatures(data)
    this.categorizeCsvPoints(data)
    return {
      rodoviasFeatures: data.rodoviasFeatures,
      pointsCategorized: data.pointsCategorized
    }
  }

  /**
   * Metodo para obter pontos que estao em um arquivo CSV. Caso for passado
   * ohandler de loading, quando a promise finalizar não será tratado o
   * loading já iniciado.
   * @param {MapaSir~GetPointsFromCsvParams} params - Os parâmetros
   * para obter os pontos do csv.
   * @returns {Promise<CsvFileResult>} Uma promessa que é resolvida com
   * uma instância representando o arquivo CSV.
   */
  getPointsFromCsv (params) {
    const { file = null, latCol = 'Latitude', lonCol = 'Logitude' } = params
    return new Promise((resolve, reject) => {
      if (file !== null) {
        Papa.parse(file, {
          skipEmptyLines: true,
          complete: (result) => {
            params.csvResult = new CsvFileResult(result, latCol, lonCol)
            resolve()
          },
          error: (err) => {
            reject(err)
          }
        })
      } else {
        reject(new Error('Não foi definido nenhum arquivo para ser lido'))
      }
    })
  }

  /**
   * Metodo para obter o filtro de rodovias apartir dos pontos do CSV.
   * @param {MapaSir~GetRodoviasFilterFromParams} params - Os parâmetros
   * para obter o filtro.
   * @returns {string} O filtro para a query das rodovias.
   */
  getRodoviasFilterFromCsvResult (params) {
    const { csvResult } = params
    let rodoviasFilter = null
    if (csvResult) {
      const rodovias = csvResult.getRodoviasDistinct()
      const valuesUfs = csvResult.getValuesDistinct('UF')
      if (rodovias && valuesUfs) {
        const ufs = valuesUfs.map(v => `'${v.toUpperCase()}'`)
        rodoviasFilter = `rodovia IN (${rodovias.join(', ')}) AND uf IN (${ufs.join(', ')})`
      }
    }
    params.rodoviasFilter = rodoviasFilter
  }

  /**
   * Metodo para obter a lista de feições de rodovias.
   * @param {MapaSir~GetRodoviasFeaturesParams} params - Os parâmetros
   * necessarios para buscar as feições das rodovias.
   * @returns {Promise<RodoviasResponse>} Uma promessa que é resolvida com
   * as feições das rodovias encontradas.
   */
  async getRodoviasFeatures (params) {
    const { rodoviasFilter } = params
    let rodoviasFeatures = null
    try {
      if (rodoviasFilter) {
        const result = await axios.get(config.sir.rodoviasLayer.host + '/ows', {
          params: {
            service: 'WFS',
            version: '1.0.0',
            request: 'GetFeature',
            typeName: config.sir.rodoviasLayer.layer,
            maxFeatures: config.sir.rodoviasLayer.maxFeatures,
            outputFormat: 'application/json',
            cql_filter: rodoviasFilter
          }
        })

        if (result.status === 200 && result.data) {
          rodoviasFeatures = this._unionFeatures(result.data)
        }
      }
    } catch (e) {
      console.error('Erro ao fazer a requisicao de obter as rodovias: ', e)
    }
    params.rodoviasFeatures = rodoviasFeatures
  }

  /**
   * Metodo para categorizar os pontos do CSV com as rodovias.
   * @param {MapaSir~CategorizeCsvResultsParams} params - Os parâmetros
   * necessários para categorizar os pontos do CSV.
   * @returns {MapaSir~CategorizedCsvPoints} Os pontos categorizados em
   * perto e longe de rodovias.
   */
  categorizeCsvPoints (params) {
    const { csvResult, rodoviasFeatures, distanceToRodovias } = params
    let pointsCategorized = null
    if (csvResult && rodoviasFeatures) {
      pointsCategorized = csvResult.toObjectArray()
        .reduce((pointsCategorized, point) => {
          const distanceResult = this._pointToMultiLineDistance({
            type: 'Point',
            coordinates: [point.coordinates.lon, point.coordinates.lat]
          }, rodoviasFeatures, { units: 'kilometers' })
          point.distance = (parseInt(distanceResult * 1000000)) / 1000
          point.inside = distanceResult < distanceToRodovias
          if (point.inside) {
            pointsCategorized.inside.push(point)
          } else {
            pointsCategorized.outside.push(point)
          }
          return pointsCategorized
        }, { inside: [], outside: [] })
    }
    params.pointsCategorized = pointsCategorized
  }

  _pointToMultiLineDistance (point, multiline, options) {
    return multiline.geometry.coordinates.reduce((minDistance, line) => {
      const feat = {
        type: 'LineString',
        coordinates: line
      }
      const dist = pointToLine(point, feat, options)
      return dist < minDistance ? dist : minDistance
    }, Number.MAX_SAFE_INTEGER)
  }

  /**
   * Método responsável por transformar uma lista de feições de multilinhas
   * em uma feição simples com a união das outras feições.
   * @param {Object} rodoviasResponse - A resposta do servidor com as feições.
   * @returns {Object} A feição com as linhas das rodovias.
   */
  _unionFeatures (rodoviasResponse) {
    return rodoviasResponse.features.reduce((acc, feat) => {
      acc.geometry.coordinates = acc.geometry.coordinates.concat(feat.geometry.coordinates)
      return acc
    }, {
      type: 'Feature',
      geometry: {
        type: 'MultiLineString',
        coordinates: []
      },
      geometry_name: 'geom'
    })
  }

}

exports.SIRController = SIRController
