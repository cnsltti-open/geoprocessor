/**
 * Classe responsável pelo resultado da leitura de um arquivo CSV.
 * @class
 * @property {Any[][]} fileData - Os dados do arquivo CSV.
 * @property {string} latCol - A coluna de latitude do arquivo CSV.
 * @property {string} lonCol - A coluna de longitude do arquivo CSV.
 */
class CsvFileResult {
  constructor (fileResult, latCol, lonCol) {
    this.fileData = fileResult.data
    this.latCol = latCol
    this.lonCol = lonCol
  }

  /**
   * Metodo para obter os dados em formato de objeto.
   * @returns {Object[]} Os dados do arquivo em um array.
   */
  toObjectArray () {
    let headers = null
    return this.fileData.reduce((acc, row) => {
      if (headers === null) {
        headers = row
      } else {
        const point = headers.reduce((item, header, i) => {
          item[header] = row[i]
          return item
        }, {})
        acc.push({
          point,
          coordinates: {
            lon: parseFloat(point[this.lonCol].replace(',', '.')),
            lat: parseFloat(point[this.latCol].replace(',', '.'))
          }
        })
      }
      return acc
    }, [])
  }

  /**
   * Metodo para obter a lista de valores distintos de uma coluna.
   * @param {string} column - A coluna que se obterá os valores.
   * @returns {Any[]} Os valores distintos da coluna.
   */
  getValuesDistinct (column) {
    const headerIndex = this.fileData[0].indexOf(column)

    if (headerIndex >= 0) {
      return this.fileData.reduce((acc, row, i) => {
        if (i > 0) {
          const val = row[headerIndex]
          if (!acc.includes(val)) {
            acc.push(val)
          }
        }
        return acc
      }, [])
    } else {
      return null
    }
  }

  /**
   * Metodo para obter a lista de valores de rodovias distintos.
   * @returns {string[]} Os valores distintos da coluna.
   */
  getRodoviasDistinct () {
    const rodoviaIndex = this.fileData[0].indexOf('Rodovia')
    const ufIndex = this.fileData[0].indexOf('UF')
    const tipoIndex = this.fileData[0].indexOf('Tipo de Rodovia')


    if (rodoviaIndex >= 0 && ufIndex >= 0 && tipoIndex >= 0) {
      return this.fileData.reduce((acc, row, i) => {
        if (i > 0) {
          const rodovia = row[rodoviaIndex]
          const uf = row[ufIndex]
          const tipo = row[tipoIndex]
          const val = `'${tipo === 'F' ? 'BR' : uf}-${rodovia}'`
          if (!acc.includes(val)) {
            acc.push(val)
          }
        }
        return acc
      }, [])
    } else {
      return null
    }
  }
}

module.exports = CsvFileResult
