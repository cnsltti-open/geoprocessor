const {GeometryType} = require('../../../../enum/GeometryType');
const request = require("request");
const polyUtil = require('polyline-encoded');

const googleURL = "https://maps.googleapis.com/maps/api";

class GoogleMapsAdapter {

  getMap(serviceMetaData, getMapParams) {
    return null;
  }

  query(serviceMetaData, queryParams) {
    return new Promise((resolve, reject) => {
      resolve(null)
    });
  }

  getCoordinatesFromAddress(serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = googleURL + '/place/autocomplete/json';
      let googleParams = {
        key: serviceMetaData.key,
        input: params.address,
        //components: 'country:br',
        language: 'pt-BR'
      };

      request({
          url,
          json: true,
          qs: googleParams
        },
        (error, response, geoCodeResponse) => {
          try {
            if (error) {
              throw error;
            }

            if (String(response.statusCode) !== '200') {
              throw new Error('Erro no consumo do Google Maps');
            }

            let {predictions} = geoCodeResponse;

            if (!predictions.length) {
              console.log('geoCodeResponse', geoCodeResponse)
              resolve({
                error: 'Não foram encontrados resultados'
              });
            }

            var placeDetailsRequest = predictions.map((prediction) => {
              return this.getPlaceDetails(serviceMetaData.key, prediction.place_id);
            });

            Promise.all(placeDetailsRequest)
              .then((results) => {
                resolve(results.filter((elem, index, result) =>{
                  return (typeof elem === 'object')
                }))
              })
              .catch((error) => reject(error));

          } catch (error) {
            reject(error);
          }

        });
    });
  }

  getPlaceDetails(key, placeid) {
    return new Promise((resolve, reject) => {
      let url = googleURL + '/place/details/json';
      let params = {placeid, key};
      request({
        url,
        json: true,
        qs: params
      }, (error, response, body) => {
        try {
          if (error) {
            throw new Error('Erro ao buscar detalhes do local (google places details)');
          } else if (body.status == "ZERO_RESULTS") {
            throw  new Error('Não foram encontrado resultados');
          } else if (body.status === "OK") {
            let result = {
              lat: body.result.geometry.location.lat,
              lng: body.result.geometry.location.lng,
              fullAddress: body.result.formatted_address,
              local: ''
            }
            for (let i=0; i < body.result.address_components.length; i++) {
              if ((body.result.address_components[i].types.indexOf('route') > -1)){
                result.local = body.result.address_components[i].long_name;
              } else if (body.result.address_components[i].types.indexOf('sublocality_level_1') > -1 || body.result.address_components[i].types.indexOf('sublocality_level_2') > -1 || body.result.address_components[i].types.indexOf('sublocality_level_3') > -1) {
                result.local = body.result.address_components[i].short_name;
              }
              if (body.result.address_components[i].types.indexOf('administrative_area_level_2') > -1){
                result.municipio = body.result.address_components[i].long_name;
              }
              if (body.result.address_components[i].types.indexOf('administrative_area_level_1') > -1){
                result.uf = body.result.address_components[i].short_name;
              }
              if (body.result.address_components[i].types.indexOf('country') > -1){
                result.pais = body.result.address_components[i].long_name;
                if (result.pais === 'Brazil') {
                  result.pais = 'Brasil';
                }
              }
            }
            resolve(result)
          } else {
            resolve('')
          }
        } catch (error){
          reject(error);
        }
      });
    })
  }

  getLatLngDetails(serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = googleURL + '/geocode/json';
      let req = {key: serviceMetaData.key, latlng:params.lat + ',' + params.lon,language: 'pt-BR'};
      request({
        url,
        json: true,
        qs: req
      }, (error, response, body) => {
        try {
          if (error) {
            throw new Error('Erro ao buscar detalhes do local (google places details)');
          } else if (body.status == "ZERO_RESULTS") {
            throw  new Error('Não foram encontrado resultados');
          } else if (body.status === "OK") {
            let result = {}
            if (body.results.length && body.results.length >0) {
              result = {
                lat: body.results[0].geometry.location.lat,
                lng: body.results[0].geometry.location.lng,
                fullAddress: body.results[0].formatted_address,
                local: ''
              }
              for (let i=0; i < body.results[0].address_components.length; i++) {
                if ((body.results[0].address_components[i].types.indexOf('route') > -1)){
                  result.local = body.results[0].address_components[i].long_name;
                } else if (body.results[0].address_components[i].types.indexOf('sublocality_level_1') > -1 || body.results[0].address_components[i].types.indexOf('sublocality_level_2') > -1 || body.results[0].address_components[i].types.indexOf('sublocality_level_3') > -1) {
                  result.local = body.results[0].address_components[i].short_name;
                }
                if (body.results[0].address_components[i].types.indexOf('administrative_area_level_2') > -1){
                  result.municipio = body.results[0].address_components[i].long_name;
                }
                if (body.results[0].address_components[i].types.indexOf('administrative_area_level_1') > -1){
                  result.uf = body.results[0].address_components[i].short_name;
                }
                if (body.results[0].address_components[i].types.indexOf('country') > -1){
                  result.pais = body.results[0].address_components[i].long_name;
                  if (result.pais === 'Brazil') {
                    result.pais = 'Brasil';
                  }
                }
              }
            }

            resolve(result)
          }
        } catch (error){
          reject(error);
        }
      });
    })
  }

  getDirections(serviceMetaData, params) {
    return new Promise((resolve, reject) => {
      let url = googleURL + '/directions/json';
      let travelMode = params.mode;
      let googleParams = {
        key : serviceMetaData.key,
        travelMode : params.mode,
        language : 'pt-BR',
        units : 'metric',
        origin : params.origin.lat + ',' + params.origin.lng,
        destination : params.destination.lat + ',' + params.destination.lng
      };
      if (params.mode.toUpperCase() === 'BUS' || params.mode.toUpperCase() === 'TRANSIT') {
        googleParams.transitOptions = 'BUS';
        googleParams.travelMode = 'TRANSIT';
      }


      for (let point of (params.waypoints || [])) {
        if (!googleParams.waypoints) {
          googleParams.waypoints = '';//optimize:true|
        } else {
          googleParams.waypoints += '|';
        }
        googleParams.waypoints += point.lat + ',' + point.lng;
      }

      request({
          url,
          method: "GET",
          qs: googleParams
        },
        (error, response, routeResponse) => {
          if (error) {
            reject(error);
          } else if (String(response.statusCode) !== '200') {
            reject(new Error('Erro no consumo do Google Maps'));
          } else {
            routeResponse = JSON.parse(routeResponse);

            console.log('GoogleMaps:getDirections#status', routeResponse.status);

            let features = [];

            for (let route of routeResponse.routes) {

              for (let leg of route.legs) {
                let feature = {
                  type: "Feature",
                  properties : {
                    distance : leg.distance,
                    duration : leg.duration,
                    start_address : leg.start_address,
                    end_address : leg.end_address
                  },
                  geometry : {
                    type : GeometryType.LINE,
                    coordinates : []
                  },
                  steps: []
                };

                for (let step of leg.steps) {
                  let polylineStr = step.polyline.points;
                  let polyline = polyUtil.decode(polylineStr);
                  feature.geometry.coordinates = feature.geometry.coordinates.concat(
                    polyline.map(coords => [coords[1], coords[0]])
                  );
                  step.geometry = {
                    type: GeometryType.LINE,
                    coordinates: polyline.map(coords => [coords[1], coords[0]])
                  };
                  delete step.polyline;
                  feature.steps.push(step)
                }
                features.push(feature);
              }
            }
            resolve(features);
          }
        });
    });
  }
}

module.exports.GoogleMapsAdapter = GoogleMapsAdapter;
