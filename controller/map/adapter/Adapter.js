const fs  = require("fs");
const path  = require("path");

let loadAdapters = (currentPath) => {
    let files = fs.readdirSync(currentPath);

    for (let fileName of files) {
        let iterPath = path.resolve(currentPath, fileName);
        if (fs.statSync(iterPath).isDirectory()) {
            let iterAdapterfiles = fs.readdirSync(iterPath);
            for (let fName of iterAdapterfiles) {
                if (/Adapter\.js/.test(fName)) {
                    adapters[fileName] = require('./' + fileName + '/' + fName)[fName.replace(/\.js/, '')];
                }
            }
        }
    }
};

let adapters = {};

loadAdapters(__dirname);

class Adapter {

    constructor(serviceType, request, response) {
        this.serviceType = serviceType;
        this.request = request;
        this.response = response;
    }

    _exec(methodName, ...args) {
        let Class = adapters[this.serviceType];
        let adapter = new Class(this.request, this.response);
        return adapter[methodName].apply(adapter, args);
    }

    query(...args) {
        return this._exec.apply(this, ['query'].concat(args));
    }

    getMap(...args) {
        return this._exec.apply(this, ['getMap'].concat(args));
    }

    getCoordinatesFromAddress(...args) {
        return this._exec.apply(this, ['getCoordinatesFromAddress'].concat(args));
    }

    getDirections(...args) {
        return this._exec.apply(this, ['getDirections'].concat(args));
    }

    getLatLngDetails(...args) {
        return this._exec.apply(this, ['getLatLngDetails'].concat(args));
    }
}

module.exports = Adapter;
