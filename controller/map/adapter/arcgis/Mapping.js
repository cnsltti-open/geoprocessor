const update = require('immutability-helper');
const {SpatialRelEnum} = require("../../../../enum/SpatialRelEnum");
const Store = require("../../../../Store");
const {GeometryType} = require("../../../../enum/GeometryType");
const ArcGISParser = require("terraformer-arcgis-parser");

const ArcGISGeometryTypeMapping = {

  [GeometryType.POLYGON]: 'esriGeometryPolygon',
  [GeometryType.LINE]: 'esriGeometryPolyline',
  [GeometryType.POINT]: 'esriGeometryPoint',
  [GeometryType.MULTI_POINT]: 'esriGeometryMultipoint',
  [GeometryType.MULTI_LINE]: 'esriGeometryPolyline'

};

const ArcGISSpatialRelMapping = {

  [SpatialRelEnum.Contains]: 'esriSpatialRelContains',
  [SpatialRelEnum.Crosses]: 'esriSpatialRelCrosses',
  [SpatialRelEnum.EnvelopeIntersects]: 'esriSpatialRelEnvelopeIntersects',
  [SpatialRelEnum.IndexIntersects]: 'esriSpatialRelIndexIntersects',
  [SpatialRelEnum.Intersects]: 'esriSpatialRelIntersects',
  [SpatialRelEnum.Overlaps]: 'esriSpatialRelOverlaps',
  [SpatialRelEnum.Relation]: 'esriSpatialRelRelation',
  [SpatialRelEnum.Touches]: 'esriSpatialRelTouches',
  [SpatialRelEnum.Within]: 'esriSpatialRelWithin',

};

const arcGISQueryParamsConvert = (appQueryParams) => {
  let updates = {
    f: {
      $set: 'geojson'
    }
  };

  if (appQueryParams.outFields) {
    updates['outFields'] = {
      $set: appQueryParams.outFields.join(', ')
    };
  } else {
    updates['outFields'] = {
      $set: '*'
    };
  }

  if (appQueryParams.where && appQueryParams.where !=='string') {
    updates['where'] = {
      $set: appQueryParams.where
    };
  } else {
    updates['where'] = {
      $set: '1=1'
    };
  }

  //converte o geometryType para o ArcGIS
  if (appQueryParams.geometryType) {
    updates['geometryType'] = {
      $set: ArcGISGeometryTypeMapping[appQueryParams.geometryType]
    };
  }

  //converte o spatialRel para o ArcGIS
  if (appQueryParams.spatialRel) {
    updates['spatialRel'] = {
      $set: ArcGISSpatialRelMapping[appQueryParams.spatialRel]
    };
  }

  if (appQueryParams.geometry) {

    if (Object.keys(appQueryParams.geometry).length>0) {
      if (!appQueryParams.geometryType) {
        throw new Error('Arcgis: geometryType está inválido');
      }
      //Se o tipo for GeoJson, converte para ESRI.
      if (appQueryParams.geometry.hasOwnProperty('coordinates')){
        appQueryParams.geometry = ArcGISParser.convert(appQueryParams.geometry);
        updates['geometry'] = {
          $set : JSON.stringify(appQueryParams.geometry)
        }
      } else {
        switch (appQueryParams.geometryType) {
          case GeometryType.POINT:
            updates['geometry'] = {
              $set: JSON.stringify({
                x: appQueryParams.geometry.lng,
                y: appQueryParams.geometry.lat
              })
            };
            break;

          case GeometryType.MULTI_POINT:
            updates['geometry'] = {
              $set: JSON.stringify({
                points: appQueryParams.geometry.map(({lat, lng}) => [lng, lat])
              })
            };
            break;
        }
      }
    } else {
      delete appQueryParams.geometry;
    }

  }

  return update(appQueryParams, updates);
};

const exportParamsRequest = (appGetMapParams) => {

  let nParams = {};
  let serviceMetaData = Store.get('service:' + appGetMapParams['serviceId']);

  let mapping = {

    set bbox(v) {
      nParams['bbox'] = appGetMapParams['bbox'];
    },

    set size(v) {
      nParams['size'] = appGetMapParams.width + ',' + appGetMapParams.height;
    },

    set dpi(v) {
      nParams['dpi'] = 200;
    },

    set format(v) {
      nParams['format'] = appGetMapParams.format;
    },

    set layers(v) {
      nParams['layers'] = 'show:' + serviceMetaData.url.match(/(MapServer|FeatureServer)\/([\d]+)\/?/)[2];
    },

    set transparent(v) {
      nParams['transparent'] = appGetMapParams.transparent;
    },

    set f(v) {
      nParams['f'] = 'image';
    },

    set inSR(v) {
      nParams['inSR'] = appGetMapParams.inSR;
    }

  };

  let keys = Object.keys(mapping);

  for (let key of keys) {
    mapping[key] = appGetMapParams;
  }

  return nParams;
};

exports.ArcGISGeometryTypeMapping = ArcGISGeometryTypeMapping;
exports.ArcGISSpatialRelMapping = ArcGISSpatialRelMapping;
exports.arcGISQueryParamsConvert = arcGISQueryParamsConvert;
exports.exportParamsRequest = exportParamsRequest;