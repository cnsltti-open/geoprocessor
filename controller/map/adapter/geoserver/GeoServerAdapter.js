const request = require("request");
const xml2js = require('xml2js')
const {GeoServerQueryParamsConvert, exportParamsRequest} = require("./Mapping");

class GeoServerAdapter {

  constructor(request, response) {
    this.request = request;
    this.response = response;
  }

  getMap(serviceMetaData, getMapParams) {
    throw new Error('Erro no consumo GeoServer:Método não implementado');
  }

  query(serviceMetaData, queryParams) {
    return new Promise((resolve, reject) => {
      let url = serviceMetaData.url.replace(/\/$/, '') + '/ows' // ?service=wfs&version=2.0.0&request=GetFeature&typeNames=namespace:';
      let params = GeoServerQueryParamsConvert(serviceMetaData, queryParams)

      request({
        url,
        method: "POST",
        headers: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        form: params
      }, (error, response, data) => {
        try {
          if (error) {
            throw error;
          }

          if (data.charAt(0) === '<') {
            xml2js.parseString(data, (err, jsData) => {
              if (err) {
                throw err
              }
              throw new Error(JSON.stringify(jsData['ows:ExceptionReport']['ows:Exception'].map(v => v['ows:ExceptionText']).join()))
            })
          } else {
            resolve(JSON.parse(data))
          }

        } catch (error) {
          console.error('Erro no consumo GeoServer:query');
          reject(error);
        }
      });
    });
  }

  getCoordinatesFromAddress(serviceMetaData, params) {
    throw new Error('Erro no consumo GeoServer:Método não implementado');
  }

  getDirections(serviceMetaData, params) {
    throw new Error('Erro no consumo GeoServer:Método não implementado');
  }
}

module.exports.GeoServerAdapter = GeoServerAdapter;
