const {EndPoint} = require("../../decorator/EndPoint");
const {HttpVerb} = require("../../enum/HttpVerb");
const {GeoprocessorController} = require("../../controller/GeoprocessorController")
const {SpatialRelEnum} = require("../../enum/SpatialRelEnum");

/**
 * /v1/api/intersect
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/intersect',
  tags: ['Geoprocessos'],
  summary: 'Verifica se uma coordenada está dentro da área geográfica testada. Retorna true caso esteja dentro e false se não estiver.',
  description: 'Testa se uma determinada coordenada (Latitude Longitude) está dentro de uma área geográfica especificada.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'boolean',
        description: 'Retorna true se estiver dentro da área e false se não estiver'
      }
    },
    '400': {
      description: 'Operação inválida',
    }
  },
  params: {
    type: {
      type: 'string',
      enum: ['BR', 'UF', 'MUN'],
      required: true,
      example: 'UF',
      description: 'Informe o tipo de área a ser pesquisada. BR para o Brasil, UF para informar a sigla de uma UF e MUN para informar um Município pelo seu nome ou código IBGE.'
    },
    area: {
      type: 'string',
      required: false,
      example: 'GO',
      description: 'Informe o valor da área de acordo com o tipo selecionado. Se deseja testar se a coordenada está dentro de um Estado, informe a sigla da UF, se quer testar se está dentro de um Município informe o nome ou código IBGE do município. Para Brasil não é preciso valor.'
    },
    lat: {
      type: 'number|string',
      required: true,
      example: -15
    },
    lon: {
      type: 'number|string',
      required: true,
      example: -47
    }
  }
})(GeoprocessorController, 'intersect');

/**
 * /v1/api/query
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/query',
  tags: ['Geoprocessos'],
  summary: 'Faz uma consulta em um serviço geográfico.',
  description: 'Faz uma consulta em um serviço geográfico de acordo com os parâmetros de filtro de atributos e/ou geográficos.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'object',
        properties: {
          "type": {
            type: 'string'
          },
          crs: {
            type: 'object'
          },
          features: {
            type: 'array',
            description: 'Array de objetos contendo chave|valor de acordo com o serviço consultado.'
          }
        },
        example: {
          "type": "FeatureCollection",
          "crs": {
            "type": "name",
            "properties": {
              "name": "EPSG:4326"
            }
          },
          "features": [
            {
              "type": "Feature",
              "id": 1,
              "geometry": {
                "type": "Point",
                "coordinates": [
                  -54.561091999999974,
                  -25.58918700026432
                ]
              },
              "properties": {
                "OBJECTID": 1,
                "descricao": "Foz do Iguaçu (BR) / Puerto Iguazú (AR)"
              }
            },
            {
              "type": "Feature",
              "id": 2,
              "geometry": {
                "type": "Point",
                "coordinates": [
                  -57.09324541799998,
                  -29.74303641418666
                ]
              },
              "properties": {
                "OBJECTID": 2,
                "descricao": "Uruguaiana (BR) / Paso de Los Libres (AR)"
              }
            }
          ]
        }
      }
    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    serviceId: {
      type: 'string',
      required: true,
      example: 'pontosDeFronteira',
      enum: [
        'pontosDeFronteira',
        'rides',
        'estados',
        'sedesMunicipiais',
        'municipios',
        'regioes',
        'limitesBrasil'
      ],
      default: 'pontosDeFronteira'
    },
    where: {
      type: 'string',
      required: false
    },
    returnGeometry: {
      type: 'boolean',
      required: false,
      default: true,
      example: true
    },
    geometry: {
      type: 'object',
      required: false,
      description: 'Geometria para ser utilizada como parâmetro de busca espacial. Verificar o método de busca "spatialRel"'
    },
    spatialRel: {
      type: 'string',
      required: false,
      enum: [
        SpatialRelEnum.Intersects,
        SpatialRelEnum.Contains,
        SpatialRelEnum.Crosses,
        SpatialRelEnum.EnvelopeIntersects,
        SpatialRelEnum.IndexIntersects,
        SpatialRelEnum.Overlaps,
        SpatialRelEnum.Touches,
        SpatialRelEnum.Within,
        SpatialRelEnum.Relation
      ],
      default: SpatialRelEnum.Intersects,
      description: 'Determina o método que será utilizado para a busca espacial utilizando como parâmetro a geometria (geometry) fornecida.'
    }
  }
})(GeoprocessorController, 'query');

/**
 * /v1/api/geocode
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['Geoprocessos'],
  summary: 'A partir de um endereço, retorna um array de posições geográficas',
  description: 'A partir de um endereço, retorna um array de posições geográficas',
  route: '/v1/api/getCoordinatesFromAddress',
  response: {
    '200': {
      description: 'Array de objetos contendo os endereços encontrados',
      schema: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            lat: {
              type: 'number'
            },
            lng: {
              type: 'number'
            },
            fullAddress: {
              type: 'string'
            },
            local: {
              type: 'string'
            },
            municipio: {
              type: 'string'
            },
            uf: {
              type: 'string'
            },
            pais: {
              type: 'string'
            }
          }
        },
        example: [
          {
            lat: -15.7933138,
            lng: -47.8826365,
            fullAddress: 'Eixo Rodoviário de Brasília - Setor Cultural Norte - Brasília, DF, 70297-400, Brazil',
            local: 'Eixo Rodoviário de Brasília',
            municipio: 'Brasília',
            uf: 'DF',
            pais: 'Brasil'

          },
          {
            lat: -15.7936263,
            lng: -47.883182,
            fullAddress: 'Eixo Rodoviário de Brasília - Brasília, DF, 70297-400, Brazil',
            local: 'Eixo Rodoviário de Brasília',
            municipio: 'Brasília',
            uf: 'DF',
            pais: 'Brasil'
          }
        ]
      }
    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    service: {
      type: 'string',
      required: true,
      enum: ['google', 'bing'],
      example: 'bing',
      description: 'Serviço de busca utilizado'
    },
    address: {
      type: 'string',
      required: true,
      example: 'Rodoviária do Plano Piloto, Brasília, DF'
    }
  }
})(GeoprocessorController, 'getCoordinatesFromAddress');

/**
 * /v1/api/getPointInfo:
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['Geoprocessos'],
  route: '/v1/api/getPointInfo',
  description: 'Retorna informações básicas de um ponto informado',
  summary: 'Retorna as informações básicas como UF, Município, Código do IBGE de um ponto informado',
  response: {
    '200': {
      description: 'Informações sobre um ponto geográfico',
      schema: {
        type: 'object',
        properties: {
          municipio: {
            type: 'string'
          },
          uf: {
            type: 'string'
          },
          estado: {
            type: 'string'
          },
          regiao: {
            type: 'string'
          },
          geocodigo: {
            type: 'number'
          },
          regiao_metropolitana: {
            type: 'string'
          },
          pais: {
            type: 'string'
          },
          codigo_un_pais: {
            type: 'string'
          },
          fullAddress: {
            type: 'string'
          }
        },
        example: {
          "municipio": "VILA BOA",
          "estado": "GOIÁS",
          "uf": "GO",
          "regiao": "CENTRO-OESTE",
          "geocodigo": 5222203,
          "regiao_metropolitana": "RIDE - Região Integrada de Desenvolvimento do Distrito Federal e Entorno",
          "pais": "Brasil",
          "codigo_un_pais": 76,
          "fullAddress": "Unnamed Road, Vila Boa - GO, 73825-000, Brazil"
        }
      }
    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    lat: {
      type: 'number|string',
      required: true,
      example: -15
    },
    lon: {
      type: 'number|string',
      required: true,
      example: -47
    },
    service: {
      type: 'string',
      required: false,
      example: 'bing'
    }
  }
})(GeoprocessorController, 'getPointInfo');

/**
 * /v1/api/directions
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['Geoprocessos'],
  route: '/v1/api/directions',
  description: 'Retorna uma rota calculada de acordo com os parâmetros informados',
  summary: 'Retorna uma rota calculada de acordo com os parâmetros informados',
  response: {
    '200': {
      description: 'Objeto geográfico da rota traçada',
      schema: {
        type: 'array',
        items: {
          type: 'object',
          description: 'Trechos das rotas',
          properties: {
            geometry: {
              type: 'object',
              properties: {
                coordinates: {
                  type: 'array',
                  description: 'Par de coordenadas de cada vértice da linha do trecho',
                  example: '[-47.88216, -15.79423],[-47.88203, -15.79383]'
                },
                'type': {
                  type: 'string',
                  description: 'Tipo de geometria (LineString)',
                  example: 'LineString'
                }
              }
            },
            'properties': {
              type: 'object',
              properties: {
                distance: {
                  type: 'object',
                  properties: {
                    text: {
                      type: 'string',
                      example: '0,2 km',
                      description: 'Texto formatado com a distância entre o ponto inicial e final'
                    },
                    value: {
                      type: 'number',
                      example: '220',
                      description: 'Valor em metros entre o ponto inicial e final'
                    }
                  }
                },
                duration: {
                  type: 'object',
                  properties: {
                    text: {
                      type: 'string',
                      example: '1 min',
                      description: 'Texto formatado com a duração estimada do percurso'
                    },
                    value: {
                      type: 'number',
                      example: '35',
                      description: 'Valor em segundos da duração estimada do percurso'
                    }
                  }
                },
                start_address: {
                  type: 'string',
                  example: 'SGAS I - Brasília, DF, 70297-400, Brasil',
                  description: 'Endereço do ponto inicial'
                },
                end_address: {
                  type: 'string',
                  example: 'Rodoviária Plano Piloto, 3368 - Centro, Brasília - DF, 70297-400, Brasil',
                  description: 'Endereço do ponto final'
                }
              }
            },
            steps: {
              type: 'array',
              description: 'Passo a passo do trecho da rota',
              items: {
                type: 'object',
                properties: {
                  geometry: {
                    type: 'object',
                    properties: {
                      coordinates: {
                        type: 'array',
                        description: 'Par de coordenadas de cada vértice da linha do trecho',
                        example: '[-47.88216, -15.79423],[-47.88203, -15.79383]'
                      },
                      'type': {
                        type: 'string',
                        description: 'Tipo de geometria (LineString)',
                        example: 'LineString'
                      }
                    }
                  },
                  distance: {
                    type: 'object',
                    properties: {
                      text: {
                        type: 'string',
                        example: '0,2 km',
                        description: 'Texto formatado com a distância do passo do trecho'
                      },
                      value: {
                        type: 'number',
                        example: '220',
                        description: 'Valor em metros da distância do passo do trecho'
                      }
                    }
                  },
                  duration: {
                    type: 'object',
                    properties: {
                      text: {
                        type: 'string',
                        example: '1 min',
                        description: 'Texto formatado com a duração para percorrer o passo do trecho'
                      },
                      value: {
                        type: 'number',
                        example: '35',
                        description: 'Valor em segundos da duração para percorrer o passo do trecho'
                      }
                    }
                  },
                  html_instructions: {
                    type: 'string',
                    description: 'Texto contendo as instruções do passo no formato HTML',
                    example: 'Siga na direção <b>norte</b>'
                  },
                  start_location: {
                    type: 'object',
                    properties: {
                      lat: {
                        type: 'number',
                        example: '-15.792347',
                        description: 'Latitude do ponto inicial do passo do trecho'
                      },
                      lng: {
                        type: 'number',
                        example: '-47.8815338',
                        description: 'Longitude do ponto inicial do passo do trecho'
                      }
                    }
                  },
                  end_location: {
                    type: 'object',
                    properties: {
                      lat: {
                        type: 'number',
                        example: '-15.792347',
                        description: 'Latitude do ponto final do passo do trecho'
                      },
                      lng: {
                        type: 'number',
                        example: '-47.8815338',
                        description: 'Longitude do ponto final do passo do trecho'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    service: {
      type: 'string',
      required: true,
      example: 'bing',
      description: 'Tipo de serviço para realização da rota'
    },
    origin: {
      type: 'object',
      required: true,
      description: 'Ponto de origem da rota',
      properties: {
        lat: {
          type: 'number|string',
          description: 'Latitude do ponto de origem',
          example: '-15.7942287'
        },
        lng: {
          type: 'number|string',
          description: 'Longitude do ponto de origem',
          example: '-47.8821658'
        }
      },
      validate: (origin) => {
        if (!origin.lat || !origin.lng) {
          throw new Error('Parâmetro \'origin\' está inválido');
        }
        return origin;
      }
    },
    destination: {
      type: 'object',
      required: true,
      description: 'Ponto de destino da rota',
      properties: {
        lat: {
          type: 'number|string',
          description: 'Latitude do ponto de destino',
          example: '-16.68689119999999'
        },
        lng: {
          type: 'number|string',
          description: 'Longitude do ponto de destino',
          example: '-49.2647943'
        }
      },
      validate: (destination) => {
        if (!destination.lat || !destination.lng) {
          throw new Error('Parâmetro \'destination\' está inválido');
        }
        return destination;
      }
    },
    waypoints: {
      type: 'array',
      required: false,
      description: 'Pontos intermediários entre a origem e o destino',
      items: {
        type: 'object',
        properties: {
          lat: {
            type: 'number|string',
            description: 'Latitude do ponto',
            example: '-16.310254530340867'
          },
          lng: {
            type: 'number|string',
            description: 'Longitude do ponto',
            example: '-48.94134521484376'
          }
        }
      },
      validate: (waypoints) => {
        for (let coordinate of waypoints) {
          if (!coordinate.lat || !coordinate.lng) {
            throw new Error('Parâmetro \'waypoints\' está inválido');
          }
        }
        return waypoints;
      }
    },
    mode: {
      type: 'string',
      required: false,
      enum: ['DRIVING', 'WALKING', 'BYCICLING', 'BUS'],
      default: 'DRIVING',
      example: 'DRIVING'
    }
  }
})(GeoprocessorController, 'getDirections');

/**
 * /v1/api/exportRoute
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/exportRoute',
  tags: ['Geoprocessos'],
  summary: 'Exporta o shapefile de um geoJSON.',
  description: 'Transforma um geoJSOn em ShapeFile.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'object',
        description: 'Rota no formato desejado',
        properties: {}
      }
    },
    '400': {
      description: 'Operação inválida',
    }
  },
  params: {
    format: {
      type: 'string',
      required: true,
      enum: ['shp', 'kml']
    },
    geoJSON: {
      type: 'object',
      required: true,
      description: 'geoJSON da rota a ser exportada.',
      properties: {
        type: {
          type: 'string',
          description: 'O tipo do geoJSON',
          example: 'FeatureCollection'
        },
        features: {
          type: 'array',
          description: 'A coleção de feições do geoJSON',
          example: '[]'
        }
      },
      validate: (geoJSON) => {
        if (!geoJSON.type || geoJSON.type !== 'FeatureCollection') {
          throw new Error('Parâmetro \'type\' está inválido')
        }
        if (!geoJSON.features || !(geoJSON.features instanceof Array)) {
          throw new Error('Parâmetro \'features\' está inválido')
        }
        return geoJSON
      }
    }
  }
})(GeoprocessorController, 'exportRoute')

/**
 * /v1/api/importRoute
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/importRoute',
  tags: ['Geoprocessos'],
  summary: 'Importa o arquivo.',
  description: 'Transforma um arquivo em geoJSON.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'object',
        description: 'GeoJSON do arquivo'
      }
    },
    '400': {
      description: 'Operação inválida'
    }
  },
  params: {
    format: {
      type: 'string',
      in: 'formData',
      required: true,
      enum: ['shp', 'kml']
    },
    file: {
      type: 'file',
      in: 'formData'
    }
  }
})(GeoprocessorController, 'importRoute');

/**
 * /v1/api/getCEPDistance
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/getCEPDistance',
  tags: ['Geoprocessos'],
  summary: 'Retorna a distância de uma rota entre 2 CEPs.',
  description: 'Retorna a distância de uma rota entre 2 CEPS.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'object',
        description: 'Informações e distância entre os CEPs informados',
        properties: {
          cepFrom: {
            type: 'string',
            description: 'CEP de Origem',
            example: '70300000'
          },
          cepTo: {
            type: 'string',
            description: 'CEP de Destino',
            example: '74083090'
          },
          addresFrom: {
            type: 'string',
            description: 'Endereço de Origem do CEP informado',
            example: '70300000'
          },
          addresTo: {
            type: 'string',
            description: 'Endereço de Destino do CEP informado',
            example: '74083090'
          },
          distance: {
            type: 'number',
            description: 'Distância em Km entre os CEPs informados',
            example: '7,6'
          },
          system: {
            type: 'string',
            description: 'Nome do sistema que solicitou a busca',
            example: 'SGA'
          },
          service: {
            type: 'string',
            description: 'Informação sobre qual serviço ou cache retornou a informação',
            example: 'cache'
          },
          dateTimeUpdate: {
            type: 'string',
            description: 'Data e hora da atualiação da informação',
            example: '05/07/2019 10:40:00'
          },
          geocodigoFrom: {
            type: 'number',
            description: 'Código do IBGE do CEP de origem informado',
            example: 232323
          },
          geocodigoTo: {
            type: 'number',
            description: 'Código do IBGE do CEP de destino informado',
            example: 23232
          }

        }
      }
    },
    '400': {
      description: 'Operação inválida'
    }
  },
  params: {
    cepFrom: {
      type: 'string',
      required: true,
      example: '70297400',
      description: 'CEP de origem'
    },
    cepTo: {
      type: 'string',
      required: true,
      example: '74620395',
      description: 'CEP de destino'
    },
    service: {
      type: 'string',
      required: true,
      enum: ['google', 'bing'],
      example: 'bing',
      description: 'Serviço de busca utilizado'
    },
    system: {
      type: 'string',
      required: true,
      enum: ['SISHAB20', 'SGA'],
      example: 'SISHAB20',
      description: 'Nome do sistema que está solicitando a busca'
    }
  }
})(GeoprocessorController, 'getCEPDistance');

/**
 * /v1/api/getCoordinatesDistance
 */
EndPoint({
  verb: [HttpVerb.POST],
  route: '/v1/api/getCoordinatesDistance',
  tags: ['Geoprocessos'],
  summary: 'Retorna a distância de uma rota entre 2 pontos geográficos.',
  description: 'Retorna a distância de uma rota entre 2 pontos geográficos.',
  response: {
    '200': {
      description: 'Operação realizada com sucesso.',
      schema: {
        type: 'object',
        description: 'Informações e distância entre os 2 pontos informados',
        properties: {
          pointFrom: {
            lat: {
              type: 'number',
              example: '-15.792347',
              description: 'Latitude de origem'
            },
            lng: {
              type: 'number',
              example: '-47.8815338',
              description: 'Longitude de origem'
            }
          },
          pointTo: {
            lat: {
              type: 'number',
              example: '-15.792347',
              description: 'Latitude de destino'
            },
            lng: {
              type: 'number',
              example: '-47.8815338',
              description: 'Longitude de destino'
            }
          },
          distance: {
            type: 'number',
            description: 'Distância em Km entre os 2 pontos informados',
            example: '7,6'
          },
          system: {
            type: 'string',
            description: 'Nome do sistema que solicitou a busca',
            example: 'SGA'
          },
          service: {
            type: 'string',
            description: 'Informação sobre qual serviço ou cache retornou a informação',
            example: 'cache'
          },
          dateTimeUpdate: {
            type: 'string',
            description: 'Data e hora da atualiação da informação',
            example: '05/07/2019 10:40:00'
          },
          geocodigoFrom: {
            type: 'number',
            description: 'Código do IBGE do CEP de origem informado',
            example: 232323
          },
          geocodigoTo: {
            type: 'number',
            description: 'Código do IBGE do CEP de destino informado',
            example: 23232
          }

        }
      }
    },
    '400': {
      description: 'Operação inválida'
    }
  },
  params: {
    pointFrom: {
      type: 'object',
      required: true,
      description: 'Latitude e longitude do ponto de origem',
      properties: {
        lat: {
          type: 'number',
          example: '-15.792347',
          description: 'Latitude de origem'
        },
        lng: {
          type: 'number',
          example: '-47.8815338',
          description: 'Longitude de origem'
        }
      },
      validate: (pointFrom) => {
        if (!pointFrom.lat || !pointFrom.lng) {
          throw new Error('Parâmetro \'ponto de origem\' está inválido');
        }
        return pointFrom;
      }

    },
    pointTo: {
      type: 'object',
      required: true,
      description: 'Latitude e longitude do ponto de destino',
      properties: {
        lat: {
          type: 'number',
          example: '-16.68689119999999',
          description: 'Latitude de destino'
        },
        lng: {
          type: 'number',
          example: '-49.2647943',
          description: 'Longitude de destino'
        }
      },
      validate: (pointTo) => {
        if (!pointTo.lat || !pointTo.lng) {
          throw new Error('Parâmetro \'ponto de destino\' está inválido');
        }
        return pointTo;
      }

    },
    service: {
      type: 'string',
      required: true,
      enum: ['google', 'bing'],
      example: 'bing',
      description: 'Serviço de busca utilizado'
    },
    system: {
      type: 'string',
      required: true,
      enum: ['SISHAB20', 'SGA'],
      example: 'SISHAB20',
      description: 'Nome do sistema que está solicitando a busca'
    }
  }
})(GeoprocessorController, 'getCoordinatesDistance');
