const {EndPoint} = require("../../../decorator/EndPoint")
const {HttpVerb} = require("../../../enum/HttpVerb")
const {SEMIURBANOController} = require('../../../controller/SEMIURBANOController')

/**
 * /v1/api/SEMIURBANO/directions
 */
EndPoint({
  verb: [HttpVerb.POST],
  tags: ['SEMIURBANO'],
  route: '/v1/api/SEMIURBANO/directions',
  description: 'Retorna uma rota calculada de acordo com os parâmetros informados',
  summary: 'Retorna uma rota calculada de acordo com os parâmetros informados',
  response: {
    '200': {
      description: 'Objeto geográfico da rota traçada',
      schema: {
        type: 'object',
        description: 'Trecho da rota',
        properties: {
          geometry: {
            type: 'object',
            properties: {
              coordinates: {
                type: 'array',
                description: 'Par de coordenadas de cada vértice da linha do trecho',
                example: '[-47.88216, -15.79423],[-47.88203, -15.79383]'
              },
              'type': {
                type: 'string',
                description: 'Tipo de geometria (LineString)',
                example: 'LineString'
              }
            }
          },
          'properties': {
            type: 'object',
            properties: {
              distance: {
                type: 'object',
                properties: {
                  text: {
                    type: 'string',
                    example: '0,2 km',
                    description: 'Texto formatado com a distância entre o ponto inicial e final'
                  },
                  value: {
                    type: 'number',
                    example: '220',
                    description: 'Valor em metros entre o ponto inicial e final'
                  }
                }
              },
              duration: {
                type: 'object',
                properties: {
                  text: {
                    type: 'string',
                    example: '1 min',
                    description: 'Texto formatado com a duração estimada do percurso'
                  },
                  value: {
                    type: 'number',
                    example: '35',
                    description: 'Valor em segundos da duração estimada do percurso'
                  }
                }
              },
              start_address: {
                type: 'string',
                example: 'SGAS I - Brasília, DF, 70297-400, Brasil',
                description: 'Endereço do ponto inicial'
              },
              end_address: {
                type: 'string',
                example: 'Rodoviária Plano Piloto, 3368 - Centro, Brasília - DF, 70297-400, Brasil',
                description: 'Endereço do ponto final'
              }
            }
          },
          steps: {
            type: 'array',
            description: 'Passo a passo do trecho da rota',
            items: {
              type: 'object',
              properties: {
                geometry: {
                  type: 'object',
                  properties: {
                    coordinates: {
                      type: 'array',
                      description: 'Par de coordenadas de cada vértice da linha do trecho',
                      example: '[-47.88216, -15.79423],[-47.88203, -15.79383]'
                    },
                    'type': {
                      type: 'string',
                      description: 'Tipo de geometria (LineString)',
                      example: 'LineString'
                    }
                  }
                },
                distance: {
                  type: 'object',
                  properties: {
                    text: {
                      type: 'string',
                      example: '0,2 km',
                      description: 'Texto formatado com a distância do passo do trecho'
                    },
                    value: {
                      type: 'number',
                      example: '220',
                      description: 'Valor em metros da distância do passo do trecho'
                    }
                  }
                },
                duration: {
                  type: 'object',
                  properties: {
                    text: {
                      type: 'string',
                      example: '1 min',
                      description: 'Texto formatado com a duração para percorrer o passo do trecho'
                    },
                    value: {
                      type: 'number',
                      example: '35',
                      description: 'Valor em segundos da duração para percorrer o passo do trecho'
                    }
                  }
                },
                html_instructions: {
                  type: 'string',
                  description: 'Texto contendo as instruções do passo no formato HTML',
                  example: 'Siga na direção <b>norte</b>'
                },
                start_location: {
                  type: 'object',
                  properties: {
                    lat: {
                      type: 'number',
                      example: '-15.792347',
                      description: 'Latitude do ponto inicial do passo do trecho'
                    },
                    lng: {
                      type: 'number',
                      example: '-47.8815338',
                      description: 'Longitude do ponto inicial do passo do trecho'
                    }
                  }
                },
                end_location: {
                  type: 'object',
                  properties: {
                    lat: {
                      type: 'number',
                      example: '-15.792347',
                      description: 'Latitude do ponto final do passo do trecho'
                    },
                    lng: {
                      type: 'number',
                      example: '-47.8815338',
                      description: 'Longitude do ponto final do passo do trecho'
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    '400': {
      description: 'Operação inválida',
    },
  },
  params: {
    service: {
      type: 'string',
      required: true,
      example: 'bing',
      description: 'Tipo de serviço para realização da rota'
    },
    origin: {
      type: 'object',
      required: true,
      description: 'Ponto de origem da rota',
      properties: {
        lat: {
          type: 'number|string',
          description: 'Latitude do ponto de origem',
          example: '-15.7942287'
        },
        lng: {
          type: 'number|string',
          description: 'Longitude do ponto de origem',
          example: '-47.8821658'
        }
      },
      validate: (origin) => {
        if (!origin.lat || !origin.lng) {
          throw new Error('Parâmetro \'origin\' está inválido');
        }
        return origin;
      }
    },
    destination: {
      type: 'object',
      required: true,
      description: 'Ponto de destino da rota',
      properties: {
        lat: {
          type: 'number|string',
          description: 'Latitude do ponto de destino',
          example: '-16.68689119999999'
        },
        lng: {
          type: 'number|string',
          description: 'Longitude do ponto de destino',
          example: '-49.2647943'
        }
      },
      validate: (destination) => {
        if (!destination.lat || !destination.lng) {
          throw new Error('Parâmetro \'destination\' está inválido');
        }
        return destination;
      }
    },
    waypoints: {
      type: 'array',
      required: false,
      description: 'Pontos intermediários entre a origem e o destino',
      items: {
        type: 'object',
        properties: {
          lat: {
            type: 'number|string',
            description: 'Latitude do ponto',
            example: '-16.310254530340867'
          },
          lng: {
            type: 'number|string',
            description: 'Longitude do ponto',
            example: '-48.94134521484376'
          }
        }
      },
      validate: (waypoints) => {
        for (let coordinate of waypoints) {
          if (!coordinate.lat || !coordinate.lng) {
            throw new Error('Parâmetro \'waypoints\' está inválido');
          }
        }
        return waypoints;
      }
    },
    mode: {
      type: 'string',
      required: false,
      enum: ['DRIVING', 'WALKING', 'BYCICLING', 'BUS'],
      default: 'DRIVING',
      example: 'DRIVING'
    }
  }
})(SEMIURBANOController, 'getDirections')
