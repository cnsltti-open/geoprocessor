const expect = require('expect');
const request = require('supertest');
const server = require("./../../../index")._server._server;


describe('geoprocessor', function () {
  describe('POST /v1/api/intersect', function () {
    it('should return true if a coordinate is inside the tested area', function (done) {
      let params = {
        type: 'UF',
        area: 'DF',
        lat: -15.762584,
        lon: -47.889778
      };
      request(server)
        .post('/v1/api/intersect')
        .send(params)
        .expect(200)
        .expect((res) => {
          expect(res.body).toBe(true);
        })
        .end(done);
    });

    it('should return false if coordinate is not inside the tested area', function (done) {
      let params = {
        type: 'UF',
        area: 'DF',
        lat: -16.094306,
        lon: -48.056674
      };
      request(server)
        .post('/v1/api/intersect')
        .send(params)
        .expect(200)
        .expect((res) => {
          expect(res.body).toBe(false);
        })
        .end(done);
    });

    describe('Missing parameters', function () {
      it('should return 400 and error if missing parameter \'tipo\'', function (done) {
        let params = {
          area: 'DF',
          lat: -16.094306,
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro \'tipo\' é obrigatório');
          })
          .end(done);
      });

      it('should return 400 and error if missing parameter \'area\'', function (done) {
        let params = {
          type: 'UF',
          lat: -16.094306,
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro \'area\' é obrigatório para tipo \'UF\'');
          })
          .end(done);
      });

      it('should return 400 and error if missing parameter \'lat\'', function (done) {
        let params = {
          type: 'UF',
          area: 'DF',
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro \'lat\' é obrigatório');
          })
          .end(done);
      });

      it('should return 400 and error if missing parameter \'lon\'', function (done) {
        let params = {
          type: 'UF',
          area: 'DF',
          lat: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro \'lon\' é obrigatório');
          })
          .end(done);
      });
    });

    describe('Invalid parameters', function () {
      it('should return 400 and error if invalid parameter \'tipo\'', function (done) {
        let params = {
          type: 10,
          area: 'DF',
          lat: -16.094306,
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro tipo está inválido');
          })
          .end(done);
      });

      it('should return 400 and error if invalid parameter \'area\'', function (done) {
        let params = {
          type: 'UF',
          area: 10,
          lat: -16.094306,
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro area está inválido');
          })
          .end(done);
      });

      it('should return 400 and error if invalid parameter \'lat\'', function (done) {
        let params = {
          type: 'UF',
          area: 'DF',
          lat: '-16.094306',
          lon: -48.056674
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro lat está inválido');
          })
          .end(done);
      });

      it('should return 400 and error if invalid parameter \'lat\'', function (done) {
        let params = {
          type: 'UF',
          area: 'DF',
          lat: -16.094306,
          lon: '-48.056674'
        };
        request(server)
          .post('/v1/api/intersect')
          .send(params)
          .expect(400)
          .expect((res) => {
            expect(res.body.error).toBe(true);
            expect(res.body.descricao).toBe('O parâmetro lon está inválido');
          })
          .end(done);
      });
    });
  });
});