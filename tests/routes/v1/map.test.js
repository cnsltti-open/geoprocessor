const expect = require('expect');
const request = require('supertest');
const server = require("./../../../index")._server._server;

before(function (done) {
  this.timeout(15000);
  server.on('appStarted', function() {
    done();
  });
});

describe('map', function() {
  describe('POST /v1/api/getCoordinatesFromAddress', function() {
    it('should return an array of geographic objects', function(done)  {
      let model = {
        service: 'google',
        address: 'Brasília'
      };
      request(server)
        .post('/v1/api/getCoordinatesFromAddress')
        .send(model)
        .expect(200)
        .expect((res) => {
          let geographicObjects = res.body;
          let singleObject = geographicObjects[0];
          expect(geographicObjects).toBeInstanceOf(Array);
          expect(singleObject).toHaveProperty('fullAddress');
          expect(singleObject).toHaveProperty('lat');
          expect(singleObject).toHaveProperty('lng');
        })
        .end(done);
    });

    it('should return 400 and error msg if missing address property', function (done) {
      request(server)
        .post('/v1/api/getCoordinatesFromAddress')
        .send({})
        .expect(400)
        .expect((res) => {
          let expectedErrorObject = {
            "error": true,
            "descricao": "O parâmetro \'address\' é obrigatório"
          };
          expect(res.body).toMatchObject(expectedErrorObject);
        })
        .end(done);
    });
    it('should return error msg if zero results', function (done) {
      request(server)
        .post('/v1/api/getCoordinatesFromAddress')
        .send({address: ''})
        .expect(200)
        .expect((res) => {
          expect(res.body).toHaveProperty('error');
        })
        .end(done);
    });
  });

});