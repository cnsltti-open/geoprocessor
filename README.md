# Instalação
- execute o comando abaixo para instalação das dependências:
> npm install

- Rode o servidor, executando:
> node index.js

- Por padrão o servidor rodará nas portas:**443** e **80**, mas caso queira mudar esta configuração
basta alterar o arquivo: *config.js* alterando a propriedade: *protocols* 

# Configuração

### config.js
Neste arquivo são mantidas as configurações do servidor do barramento de serviços geográficos. 

O atributo **services** é um array de objetos que contem as informações dos serviços externos consumidos pelo barramento de serviços.

Os serviços são hospedados em servidores ArcGIS Server ou disponibilizados na internet como o Google Directions.

Ao excluir um elemento deste array, será necessário alterar o código que o utiliza.

# Documentação

A documentação da API está disponível ao acessar o endereço **http://IP-SERVIDOR/api-doc/