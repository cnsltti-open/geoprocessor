
exports.SpatialRelEnum = {
    Intersects : "Intersects",
    Contains : "Contains",
    Crosses : "Crosses",
    EnvelopeIntersects : "EnvelopeIntersects",
    IndexIntersects : "IndexIntersects",
    Overlaps : "Overlaps",
    Touches : "Touches",
    Within : "Within",
    Relation : "Relation"
};