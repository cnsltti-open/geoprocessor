
exports.GeometryType = {

    POLYGON : 'Polygon',

    POINT  : 'Point',

    MULTI_POINT  : 'MultiPoint',

    LINE : 'LineString',

    MULTI_LINE : 'MultiLine',

    MULTI_POLYGON: 'MultiPolygon'

};
