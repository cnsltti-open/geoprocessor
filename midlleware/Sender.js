
module.exports = class Sender {

    constructor(request, response) {
        this.request = request;
        this.response = response;
        this._chunked = false;
    }

    setHeader(headerName, value) {
        this.response.setHeader(headerName, value);
    }

    error(error, statusCode = 400) {
        this.response.status(statusCode).json({
            error : true,
            descricao : error.message
        });
    }

    write(chunk) {
        this._chunked = true;
        this.response.write(chunk);
    }

    end(result) {
        if (this._chunked) {
            this.response.end();
        } else {
            this.response.json(result);
        }
    }

};