
const messages = {

    M0001 : "Destino Final deve estar em local(is) com a UF diferente do Local de Origem",
    M0002 : `Parâmetro 'waypoints' está inválido. O parâmetro: 'tipo' de um outro ponto está inválido, os valores aceitos são:
    embarque, intermediario, parada`,
    M0003 : 'É permitido inserir "Local de Embarque" somente em local(is) dentro da mesma UF do Local de Origem',
    M0004 : 'Não existem rotas entre o ponto fora do Brasil até um ponto de fronteira',
    M0005 : 'Não existem rotas disponíves',
    M0006 : 'Os pontos de destino (final e intermediário) só podem ser colocados em UFs diferentes da origem',
    M0007 : 'Não foi possível obter as UF\'s que intersectam com os pontos centrais da rota',
    M0008 : "Nenhum município encontrado",
    M0009 : "Formato não suportado",
    M0010 : "GeoJSON não pode ser nulo",
    M0011 : "Não foi possível exportar o arquivo",
    M0012 : "Arquivo kml é obrigatório",
    M0013 : "Arquivo shp é obrigatório",
    M0014 : "Arquivo não suportado",
    M0015 : "Arquivo shapefile inválido",
    M0016 : "CEP de origem não encontrado",
    M0017 : "CEP de destino não encontrado",
    M0018 : "Dados do município de origem não encontrado",
    M0019 : "Dados do município de destino não encontrado"
};

exports.messages = messages;
