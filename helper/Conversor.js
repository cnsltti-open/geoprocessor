const crypto = require("crypto");

const Conversor = {

    createHash(...value) {
        let str = JSON.stringify(value);
        let md5sum = crypto.createHash('md5');
        md5sum.update(str);
        return md5sum.digest('hex');
    }

};

module.exports = Conversor;