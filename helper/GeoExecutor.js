const Store = require("../Store");
const path = require("path");
const Adapter = require("../controller/map/adapter/Adapter");
const Cache = require("./Cache")

const pathData = path.resolve(serverPath, 'data');

module.exports = class GeoExecutor {

  constructor (request, response) {
    this.request = request;
    this.response = response;
  }

  exec (serviceId, methodName, params, generateCache = false) {
    return new Promise(async (resolve, reject, notify) => {
      let serviceMetaData = Store.get('service:' + (serviceId.id || serviceId));
      if (generateCache) {
        let cache = await Cache.getCache(methodName, params)
        if (cache) {
          if (this.response) {
            this.response.setHeader('content-type', 'application/json');
          }
          resolve(cache)
          return
        }
      }

      let adapter = new Adapter(serviceMetaData.serviceType, this.request, this.response);
      return adapter[methodName](serviceMetaData, params)
        .then(data => {
          if (generateCache) {
            Cache.putCache(methodName, params, data)
          }
          resolve(data);
        }, reject, notify)
        .catch(reject);
    });
  }

};
