const Parametro = require("../db/model/parametro")
const bdCache = require("../db/model/cache")
const Conversor = require("./Conversor");

class Cache {
  async getCache(methodName, params,) {
    let hash = Conversor.createHash(methodName, params);
    let data = new Date()
    data = new Date(data.getTime() + data.getTimezoneOffset() * 60000)
    let cache = await bdCache.findOne({hash: hash, expira: {$gte: new Date()}})
    if (cache && cache.info) {
      console.log('Cache: hash -> ' + hash)
      return cache.info
    } else {
      let re = await bdCache.deleteMany({hash: hash})
      return
    }
  }

  async putCache(methodName, params, data) {
    let hash = Conversor.createHash(methodName, params)
    let parametro = await Parametro.findOne({})
    let tempo = (parametro && parametro.tempoCache >=0) ? parametro.tempoCache : 360
    let d = new Date()
    d.setSeconds(d.getSeconds() + tempo)
    data.service = 'cache'
    bdCache.create({
      hash: hash,
      info: data,
      expira: d
    })
    console.log('Inseriu Cache: hash -> ' + hash)
    return hash
  }
}

module.exports =  new Cache()
