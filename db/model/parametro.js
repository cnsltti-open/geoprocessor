const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ParametroSchema = new Schema({
  tempoCache: {
    type: Number,
    default: 2592000 // 30 dias em segundos
  }
});
const Parametro = mongoose.model("Parametro", ParametroSchema);

Parametro.find({}).then(async (err, docs) =>{
  if (!err || err.length <= 0 ) {
    Parametro.create({
      tempoCache: 2592000 // 30 dias
    })
  }
})

module.exports = Parametro;
