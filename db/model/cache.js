const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const CacheSchema = new Schema({
  hash: {
    type: String,
    required: [true, "Hash do cache"]
  },
  info: {
    type: Object
  },
  expira: {
    type: Date
  }
});
CacheSchema.index({ hash: 1})
const Cache = mongoose.model("Cache", CacheSchema);
module.exports = Cache;
