const config = require('../database.json')
const mongoose = require('mongoose')

let uri = 'mongodb://'
if (config.user && config.password){
  uri = `${uri}${config.user}:${config.password}@`
}

uri = `${uri}${config.host}:${config.port}/${config.database}`

async function connect() {
  mongoose.connect(uri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    poolSize: 20,
    socketTimeoutMS: 480000,
    keepAlive: 300000,
    reconnectTries: 30,
    reconnectInterval: 5000
  });
  const db = mongoose.connection;
  db.on('error', console.error.bind(console, 'Erro de conexão com o MongoDB:'));
  db.once('open', function() {
    console.log('Conexão estabelecida com o MongoDB')
  });
  db.on('disconnected', () => { console.log('-> Conexão com o MongoDB perdida'); });
  db.on('reconnect', () => { console.log('-> Reconectado com MongoDB'); });
  db.on('connected', () => { console.log('-> Conectado com MongoDB'); });
  db.on('reconnectFailed', () => {
    console.log('-> Desistindo de reconectar');
    process.exit(1);
  });
  return db
}


module.exports = async function() {
  return await connect(uri)
}
