const Server = require("./Server").Server;
const cluster = require('cluster');
const OS = require('os');
const config = require("./config");

global['serverPath'] = __dirname;

buildServer(process.env.NODE_ENV === 'production' && config.cluster);

function buildServer(fork = false) {
    let instanceServer;

    if (fork) {
        if (cluster.isMaster) {
            let cpuCount = OS.cpus().length;
            let i = 0;

            while (i < cpuCount) {
                cluster.fork();
                i += 1;
            }

        } else {
            instanceServer = new Server();
        }

    } else {
        instanceServer = new Server();
    }

    exports._server = instanceServer;
    exports.serverPath = __dirname;

    global['server'] = instanceServer;
}
